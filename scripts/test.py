import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.MUO.muCorrections import muSFRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"
get_test_file = lambda x: os.path.expandvars(os.path.join(test_folder, x))

import pandas as pd

import argparse
parser = argparse.ArgumentParser(description='Plotter options')
parser.add_argument('-s','--save', action='store_true', default=False,
    help="Stores test results in a pickle file")
parser.add_argument('-c','--check', action='store_true', default=False,
    help="Compares test results with previously stored results")
options = parser.parse_args()

def muon_test(df, isMC, year, runPeriod, isUL=False):
    musf = muSFRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
        wps=["tight_id", "tight_iso"],
        isUL=isUL
    )()
    df, _ = musf.run(df)
    h = df.Histo1D("musf_tight_id")
    print(f"Muon {year}{runPeriod} tightID SF Integral: %.3f, Mean: %.3f, Std: %.3f" %
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df, (h.Integral(), h.GetMean(), h.GetStdDev())


if __name__ == "__main__":
    results = []

    df_mc2016preVFP = ROOT.RDataFrame("Events", get_test_file("testfile_mc2016.root"))
    _, res = muon_test(df_mc2016preVFP, True, 2016, "preVFP", True)
    results.append(("2016_preVFP", res[0], res[1], res[2]))

    df_mc2016postVFP = ROOT.RDataFrame("Events", get_test_file("testfile_mc2016_apv.root"))
    _, res = muon_test(df_mc2016postVFP, True, 2016, "postVFP", True)
    results.append(("2016_postVFP", res[0], res[1], res[2]))

    df_mc2017 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2017.root"))
    _, res = muon_test(df_mc2017, True, 2017, "", True)
    results.append(("2017", res[0], res[1], res[2]))

    df_mc2018 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2018.root"))
    _, res = muon_test(df_mc2018, True, 2018, "", True)
    results.append(("2018", res[0], res[1], res[2]))

    df_mc2022 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2022.root"))
    _, res = muon_test(df_mc2022, True, 2022, "preEE")
    results.append(("2022", res[0], res[1], res[2]))

    df_mc2022_postEE = ROOT.RDataFrame("Events", get_test_file("testfile_mc2022_postee.root"))
    _, res = muon_test(df_mc2022_postEE, True, 2022, "postEE")
    results.append(("2022_postEE", res[0], res[1], res[2]))

    df_mc2023 = ROOT.RDataFrame("Events", get_test_file("testfile_mc2023.root"))
    _, res = muon_test(df_mc2023, True, 2023, "preBPix")
    results.append(("2023", res[0], res[1], res[2]))

    df_mc2023_postBpix = ROOT.RDataFrame("Events", get_test_file("testfile_mc2023_postbpix.root"))
    _, res = muon_test(df_mc2023_postBpix, True, 2023, "postBPix")
    results.append(("2023_postBpix", res[0], res[1], res[2]))

    pd_df = pd.DataFrame(results)

    results_path = os.path.expandvars("$CMSSW_BASE/src/Corrections/MUO/scripts/results.pickle")
    if options.save:
        pd_df.to_pickle(results_path)
    elif options.check:
        stored_df = pd.read_pickle(results_path)
        pd_matches_with_saved_df = pd_df.equals(stored_df)
        if not pd_matches_with_saved_df:
            raise ValueError("Test results obtained do not agree with the stored results. If this "
                "is expected, please execute locally python3 test.py -s to overwrite the "
                "stored results and upload the resulting file")
