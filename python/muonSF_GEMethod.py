import os

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

json_path = "/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/MUO/{}/muon_HighPt.json.gz"


class MuonSFproducer():
    def __init__(self, *args, **kwargs):
        self.year =   kwargs.pop("year")
        self.isMC =   kwargs.pop("isMC")
        self.preVFP = kwargs.pop("preVFP")
        self.postEE = kwargs.pop("postEE")

        if self.isMC:
            ### Run 2 ###
            if self.year == 2018:
                self.tag = "2018_UL"
            elif self.year == 2017:
                self.tag = "2017_UL"
            elif self.year == 2016:
                if self.preVFP:
                    self.tag = "2016preVFP_UL"
                else:
                    self.tag = "2016postVFP_UL"
            ### Run 3 ###
            elif self.year == 2022:
                if self.postEE:
                    self.tag = "2022EE_27Jun2023"
                else:
                    self.tag = "2022_27Jun2023"
            else:
                raise ValueError("Only implemented for Run 2 UL & Run 3 2022")

            filename = json_path.format(self.tag)

            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")
            ROOT.gInterpreter.Declare(os.path.expandvars(
                '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

            # Declaring four objects: for HighPtId, for LooseRelTkIso, for HLT and for Global RECO
            # Of course this should be changed if more WPs are needed.
            ROOT.gInterpreter.ProcessLine(
                'auto id_SF = '
                    'MyCorrections("%s", "NUM_HighPtID_DEN_GlobalMuonProbes");'
                % filename)
            ROOT.gInterpreter.ProcessLine(
                'auto iso_SF = '
                    'MyCorrections("%s", "NUM_probe_LooseRelTkIso_DEN_HighPtProbes");'
                % filename)
            ROOT.gInterpreter.ProcessLine(
                'auto reco_SF = '
                    'MyCorrections("%s", "NUM_GlobalMuons_DEN_TrackerMuonProbes");'
                % filename)
            ROOT.gInterpreter.ProcessLine(
                'auto hlt_SF = '
                    'MyCorrections("%s", "NUM_HLT_DEN_HighPtLooseRelIsoProbes");'
                % filename)

            ROOT.gInterpreter.Declare("""
                using Vfloat = const ROOT::RVec<float>&;
                using Vint = const ROOT::RVec<int>&;

                double get_mu_idSF(std::string syst, float eta, float TuneP_relPt, float pt) {
                    double sf;
                    float TuneP_pt = TuneP_relPt*pt;
                    if(TuneP_pt < 50.0)
                       sf = id_SF.eval({fabs(eta), 51.0, syst});
                    else
                       sf = id_SF.eval({fabs(eta), TuneP_pt, syst});
                    return sf;
                }
                double get_mu_isoSF(std::string syst, float eta, float TuneP_relPt, float pt) {
                    double sf;
                    float TuneP_pt = TuneP_relPt*pt;
                    if(TuneP_pt < 50.0)
                       sf = iso_SF.eval({fabs(eta), 51.0, syst});
                    else
                       sf = iso_SF.eval({fabs(eta), TuneP_pt, syst});
                    return sf;
                }
                double get_mu_hltSF(std::string syst, float eta, float TuneP_relPt, float pt) {
                    double sf;
                    float TuneP_pt = TuneP_relPt*pt;
                    if(TuneP_pt < 50.0)
                       sf = hlt_SF.eval({fabs(eta), 51.0, syst});
                    else
                       sf = hlt_SF.eval({fabs(eta), TuneP_pt, syst});
                    return sf;
                }
                double get_mu_recoSF(std::string syst, float eta, float TuneP_relPt, float pt) {
                    double sf;
                    float TuneP_pt = TuneP_relPt*pt;
                    float p = TuneP_pt*cosh(eta);
                    if(p < 50.0)
                       sf = reco_SF.eval({fabs(eta), 51.0, syst});
                    else
                       sf = reco_SF.eval({fabs(eta), p, syst});
                    return sf;
                }

                double get_idSF_weight(std::string syst, Vfloat eta, Vfloat TuneP_relPt, Vfloat pt, int mu1_idx, int mu2_idx) {
                    double mu1_sf, mu2_sf, weight;
                    mu1_sf = get_mu_idSF(syst, eta[mu1_idx], TuneP_relPt[mu1_idx], pt[mu1_idx]);
                    mu2_sf = get_mu_idSF(syst, eta[mu2_idx], TuneP_relPt[mu2_idx], pt[mu2_idx]);
                    return weight = mu1_sf*mu2_sf;
                }
                double get_isoSF_weight(std::string syst, Vfloat eta, Vfloat TuneP_relPt, Vfloat pt, int mu1_idx, int mu2_idx) {
                    double mu1_sf, mu2_sf, weight;
                    mu1_sf = get_mu_isoSF(syst, eta[mu1_idx], TuneP_relPt[mu1_idx], pt[mu1_idx]);
                    mu2_sf = get_mu_isoSF(syst, eta[mu2_idx], TuneP_relPt[mu2_idx], pt[mu2_idx]);
                    return weight = mu1_sf*mu2_sf;
                }
                double get_hltSF_weight(std::string syst, Vfloat eta, Vfloat TuneP_relPt, Vfloat pt, int mu1_idx, int mu2_idx) {
                    double mu1_sf, mu2_sf, weight;
                    mu1_sf = get_mu_hltSF(syst, eta[mu1_idx], TuneP_relPt[mu1_idx], pt[mu1_idx]);
                    mu2_sf = get_mu_hltSF(syst, eta[mu2_idx], TuneP_relPt[mu2_idx], pt[mu2_idx]);
                    return weight = mu1_sf + mu2_sf - mu1_sf*mu2_sf;
                }
                double get_recoSF_weight(std::string syst, Vfloat eta, Vfloat TuneP_relPt, Vfloat pt, int mu1_idx, int mu2_idx) {
                    double mu1_sf, mu2_sf, weight;
                    mu1_sf = get_mu_recoSF(syst, eta[mu1_idx], TuneP_relPt[mu1_idx], pt[mu1_idx]);
                    mu2_sf = get_mu_recoSF(syst, eta[mu2_idx], TuneP_relPt[mu2_idx], pt[mu2_idx]);
                    return weight = mu1_sf*mu2_sf;
                }
            """)


    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        for corr in ["idSF", "isoSF", "hltSF", "recoSF"]:
            for syst_name, syst in [("", "nominal"), ("_up", "systup"), ("_down", "systdown")]:
                df = df.Define("mu_%s_weight%s" % (corr, syst_name),
                               'get_%s_weight("%s", Muon_eta, Muon_tunepRelPt, Muon_pt, mu1_index, mu2_index)' % (corr, syst))
                branches.append("mu_%s_weight%s" % (corr, syst_name))

        return df, branches


def MuonSF(**kwargs):
    """
    Module to obtain high-pt di-muon SFs with their uncertainties for: reco, ID, iso & trigger.
    Implemented for UL Run 2 (2016, 2017 & 2018) & Run 3 2022.
    All SFs are taken from official JSONs.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: MuonSF
            path: Corrections.MUO.muonSF_GEMethod
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                preVFP: self.dataset.has_tag('preVFP')
                postEE: self.dataset.has_tag('postEE')
    """

    return lambda: MuonSFproducer(**kwargs)

